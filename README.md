# JSOS .ics timetable files fixer
This script is aimed to fix .ics timetable files generated with jsos.pwr.edu.pl website.

### Requirements
Python 2.7.x

### Usage
./fixtz.py original_timetable.ics fixed_timetable.ics
or
python fixtz.py original_timetable.ics fixed_timetable.ics
