#!/usr/bin/python
import sys
import re

def fix_callendar(f):
    pattern = re.compile('(D.+;TZID=\w+/\w+:.+)Z')
    return re.sub(pattern, r'\1', f.read())

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print "Wrong number of arguments."
        print "Run with: ./{} CALENDAR_NAME.ics NEW_CALENDAR.ics".format(sys.argv[0])
        sys.exit()
    else:
        with open(sys.argv[1], "r") as f:
            fixed = fix_callendar(f)
            with open(sys.argv[2], "w+") as tf:
                tf.write(fixed)
